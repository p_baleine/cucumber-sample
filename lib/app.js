var express = require('express');
var multiline = require('multiline');
var app = express();

app.get('/', function(req, res) {
  res.send(multiline(function() {/*
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <p>Hello, cucumber</p>
  </body>
</html>
  */}));
});

module.exports = app;
