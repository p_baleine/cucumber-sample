var World = require('../support/world').World;
var expect = require('chai').expect;

module.exports = function() {
  this.World = World;

  this.Given(/^トップページへアクセスする$/, function (callback) {
    this.visit('/', callback);
  });

  this.Then(/^「Hello, cucumber」と表示される$/, function (callback) {
    expect('p').dom.to.contain.text('Hello, cucumber')
      .then(function() {
        callback();
      });
  });
};
