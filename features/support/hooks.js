module.exports = function() {
  this.Before(function(callback) {
    this.startDriver();
    this.bootServer(callback);
  });

  this.After(function(callback) {
    this.quitDriver();
    this.shutdownServer(callback);
  });
};
