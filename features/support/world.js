var url = require('url');
var sw = require('selenium-webdriver');
var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
var server = new SeleniumServer('~/bin/selenium-server-standalone-2.40.0.jar', { port: 4444 });
var chai = require('chai');
var chaiWebdriver = require('chai-webdriver');
var PORT = 3001;

function World(callback) {
  this.app = require('../../lib/app');
  this.server = null;
  callback();
}

// サーバ起動
World.prototype.bootServer = function(callback) {
  this.server = this.app.listen(PORT, callback);
};

// サーバシャットダウン
World.prototype.shutdownServer = function(callback) {
  this.server.close(callback);
};

// webdriver起動
World.prototype.startDriver = function() {
  server.start();
  this.driver = new sw.Builder()
    .usingServer(server.address())
    .withCapabilities(sw.Capabilities.chrome())
    .build();
  chai.use(chaiWebdriver(this.driver));
};

// webdriver終了
World.prototype.quitDriver = function() {
  this.driver.quit();
};

// `path`にブラウザでアクセスする
World.prototype.visit = function(path, callback) {
  this.driver.get(this.url(path));
  callback();
};

// urlを返却する
World.prototype.url = function(path) {
  return url.format({
    protocol:'http',
    hostname: 'localhost',
    port: PORT,
    pathname: path
  });
};

exports.World = World;
